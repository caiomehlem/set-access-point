#!/bin/bash

set -eu

e_err()
{
	echo >&2 "ERROR: ${*}"
}

usage()
{
	echo "Usage: ${0} -s <ssid> [-i <interface>]"
	echo
	echo "Scan all WiFi access points broadcasting under SSID and configure the connection"
	echo "to the strongest AP by fixing it's MAC address"
	echo "    -h              Print usage"
	echo "    -i <interface>  WiFi network interface"
	echo "    -s <ssid>       SSID of the network to connect to"
}


while getopts "hi:s:" options; do
	case "${options}" in
	h)
		usage
		exit 0
		;;
	i)
		interface="${OPTARG}"
		;;
	s)
		ssid="${OPTARG}"
		;;
	:)
		e_err "Option -${OPTARG} requires an argument."
		exit 1
		;;
	?)
		echo "Invalid option: -${OPTARG}"
		echo
		usage
		exit 1
		;;
	esac
done
shift "$((OPTIND - 1))"

interface="${interface:-wlp2s0}"

if [ -z "${ssid}" ]; then
	e_err "SSID option is mandatory!"
	usage
	exit 1
fi

echo "Scanning for SSID ${ssid} via interface ${interface}..."
output="$(sudo iwlist ${interface} scan 2>&1)"

# Get the number of lines of output
n_lines="$(echo -n "${output}" | grep -c '^')"
if [ ${n_lines} -eq 1 ]; then
	e_err "Interface ${interface} may not exist!"
	exit 1
fi

# Arrays to store the networks
declare -a network_quality
declare -a network_quality2
declare -a network_macs
network_count=0

while IFS= read -r line; do
	## Test line content and parse as required
	[[ "${line}" =~ "Address" ]] && mac="${line##*ss: }"
	[[ "${line}" =~ "Quality" ]] && {
		qual="${line##*ity=}"
		qual2="${qual%% *}"
		qual="${qual%%\/*}"
	}
	[[ "${line}" =~ "ESSID" ]] && {
		essid="${line##*ID:\"}"
		essid="${essid%\"}"
		# Add to the arrays after ESSID
		if [ "${essid}" = "${ssid}" ]; then
			network_quality[$network_count]="${qual}"
			network_quality2[$network_count]="${qual2}"
			network_macs[$network_count]="${mac}"
			network_count="$((network_count+1))"
		fi
	}
done <<< "${output}"

if [ $network_count -eq 0 ]; then
	e_err "No networks found for SSID \"${ssid}\""
	exit 1
fi

echo "Found ${network_count} access point(s) for network \"${ssid}\""

highest_idx=0

# Find the highest quality
for i in ${!network_quality[*]}; do
	if [ ${network_quality[$i]} -gt ${network_quality[$highest_idx]} ]; then
		highest_idx=$i
	fi
done

mac="${network_macs[$highest_idx]}"
qual="${network_quality2[$highest_idx]}"

while true; do
	read -p "Do you want to fix the WiFi BSSID property of \"${ssid}\" to \"${mac}\" with the strength ${qual}? " yn
	case "${yn}" in
		[Yy]* )
			nmcli c modify "${ssid}" 802-11-wireless.bssid "${mac}"
			break
			;;
		[Nn]* )
			echo "Aborting"
			exit
			;;
		* )
			echo "Please answer yes or no"
			;;
	esac
done
echo "Done!"

while true; do
	read -p "Do you want to reconnect to network \"${ssid}\"? " yn
	case "${yn}" in
		[Yy]* )
			nmcli c up "${ssid}"
			break
			;;
		[Nn]* )
			exit
			;;
		* )
			echo "Please answer yes or no"
			;;
	esac
done
echo "Done!"

exit 0
